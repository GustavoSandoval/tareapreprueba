package com.Gus.demo.apirest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usuario_cajero")
public class Usuario_cajero {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)

    @Column(name = "id")
    private int id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="rut")
    private int rut;

    @Column(name="usuario")
    private String usuario;

    @Column(name="contraseña")
    private String contraseña;

    @Column(name="ventas")
    private int ventas;

    public Usuario_cajero() {
    }
    public Usuario_cajero(int id,String nombre, int rut, String usuario, String contraseña, int ventas) {
        this.id = id;
        this.nombre = nombre;
        this.rut = rut;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.ventas = ventas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }

    @Override
    public String toString() {
        return "Usuario_cajero{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", rut=" + rut +
                ", usuario='" + usuario + '\'' +
                ", contraseña='" + contraseña + '\'' +
                ", ventas=" + ventas +
                '}';
    }
}