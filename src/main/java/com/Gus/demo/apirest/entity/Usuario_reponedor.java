package com.Gus.demo.apirest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="usuario_reponedor")
public class Usuario_reponedor {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)

    @Column(name = "id")
    private int id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="rut")
    private int rut;

    @Column(name="usuario")
    private String usuario;

    @Column(name="contraseña")
    private String contraseña;

    @Column(name="seccion")
    private String seccion;

    public Usuario_reponedor() {
    }

    public Usuario_reponedor(int id, String nombre, int rut, String usuario, String contraseña, String seccion) {
        this.id= id;
        this.nombre = nombre;
        this.rut = rut;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.seccion = seccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    @Override
    public String toString() {
        return "usuario_reponedor{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", rut=" + rut +
                ", usuario='" + usuario + '\'' +
                ", contraseña='" + contraseña + '\'' +
                ", seccion='" + seccion + '\'' +
                '}';
    }
}