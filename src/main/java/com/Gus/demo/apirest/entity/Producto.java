package com.Gus.demo.apirest.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




    @Entity
    @Table(name="producto")
    public class Producto {

        @Id
        @GeneratedValue(strategy=GenerationType.IDENTITY)
        @Column(name = "id")
        private int id;

        @Column(name="nombre")
        private String nombre;

        @Column(name="seccion")
        private String seccion;

        @Column(name="precio")
        private int precio;

        @Column(name="ubicacion")
        private String ubicacion;


        public Producto() {}



        public Producto(int id, String nombre, String seccion, int precio, String ubicacion) {
            this.id = id;
            this.nombre = nombre;
            this.seccion = seccion;
            this.precio = precio;
            this.ubicacion = ubicacion;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getSeccion() {
            return seccion;
        }

        public void setSeccion(String seccion) {
            this.seccion = seccion;
        }

        public int getPrecio() {
            return precio;
        }

        public void setPrecio(int precio) {
            this.precio = precio;
        }

        public String getUbicacion() {
            return ubicacion;
        }

        public void setUbicacion(String ubicacion) {
            this.ubicacion = ubicacion;
        }

        @Override
        public String toString() {
            return "Producto{" +
                    "id=" + id +
                    ", nombre='" + nombre + '\'' +
                    ", seccion='" + seccion + '\'' +
                    ", precio=" + precio +
                    ", ubicacion='" + ubicacion + '\'' +
                    '}';
        }
    }



