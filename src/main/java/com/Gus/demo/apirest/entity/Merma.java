package com.Gus.demo.apirest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="merma")
public class Merma {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name="nombre")
    private String nombre;

    @Column(name="motivo")
    private String motivo;

    @Column(name="cantidad")
    private int cantidad;



    public Merma() {
    }
    public Merma(String nombre, String motivo, int cantidad) {
        this.nombre = nombre;
        this.motivo = motivo;
        this.cantidad = cantidad;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "Merma{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", motivo='" + motivo + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}