package com.Gus.demo.apirest.dao;

import com.Gus.demo.apirest.entity.Producto;


import java.util.List;

public interface ProductoDao {

    public List<Producto> findAll();
    public Producto findById(int id);

    public void save(Producto producto);

    public void deleteById(int id);
}
