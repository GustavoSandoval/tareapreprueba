package com.Gus.demo.apirest.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.Gus.demo.apirest.entity.Merma;
import com.Gus.demo.apirest.entity.Usuario_reponedor;
import com.Gus.demo.apirest.entity.Usuario_cajero;
import com.Gus.demo.apirest.entity.Producto;

public class entity_manager {

    @Repository
    public class mermaDaoImp implements mermaDao {

        @Autowired
        private EntityManager entityManager;

        @Override
        public List<Merma> findAll() {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Merma> theQuery = currentSession.createQuery("from Merma", Merma.class);

            List<Merma> mermas = theQuery.getResultList();

            return mermas;

        }

        @Override
        public Merma findById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Merma merma = currentSession.get(Merma.class, id);

            return merma;
        }



        @Override
        public void save(Merma merma) {
            Session currentSession = entityManager.unwrap(Session.class);

            currentSession.saveOrUpdate(merma);

        }

        @Override
        public void deleteById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Merma> theQuery = currentSession.createQuery("delete from Merma where id=:idUser");

            theQuery.setParameter("idUser", id);
            theQuery.executeUpdate();

        }
    }
    @Repository
    public class productoDaoImp implements ProductoDao {

        @Autowired
        private EntityManager entityManager;

        @Override
        public List<Producto> findAll() {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Producto> theQuery = currentSession.createQuery("from Producto", Producto.class);

            List<Producto> productos = theQuery.getResultList();

            return productos;

        }

        @Override
        public Producto findById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Producto producto = currentSession.get(Producto.class, id);

            return producto;
        }



        @Override
        public void save(Producto producto) {
            Session currentSession = entityManager.unwrap(Session.class);

            currentSession.saveOrUpdate(producto);

        }

        @Override
        public void deleteById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Merma> theQuery = currentSession.createQuery("delete from Producto where id=:idUser");

            theQuery.setParameter("idUser", id);
            theQuery.executeUpdate();

        }
    }
    @Repository
    public class Usuario_cajero_DaoImp implements Usuario_cajeroDao {

        @Autowired
        private EntityManager entityManager;

        @Override
        public List<Usuario_cajero> findAll() {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Usuario_cajero> theQuery = currentSession.createQuery("from Usuario_cajero", Usuario_cajero.class);

            List<Usuario_cajero> usuario_cajeros = theQuery.getResultList();

            return usuario_cajeros;

        }

        @Override
        public Usuario_cajero findById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Usuario_cajero usuario_cajero = currentSession.get(Usuario_cajero.class, id);

            return usuario_cajero;
        }



        @Override
        public void save(Usuario_cajero usuario_cajero) {
            Session currentSession = entityManager.unwrap(Session.class);

            currentSession.saveOrUpdate(usuario_cajero);

        }

        @Override
        public void deleteById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Merma> theQuery = currentSession.createQuery("delete from Usuario Cajero where id=:idUser");

            theQuery.setParameter("idUser", id);
            theQuery.executeUpdate();

        }
    }
    @Repository
    public class usuario_reponedorDaoImp implements Usuario_reponedorDao {

        @Autowired
        private EntityManager entityManager;

        @Override
        public List<Usuario_reponedor> findAll() {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Usuario_reponedor> theQuery = currentSession.createQuery("from Usuario_reponedor", Usuario_reponedor.class);

            List<Usuario_reponedor> usuario_reponedores = theQuery.getResultList();

            return usuario_reponedores;

        }

        @Override
        public Usuario_reponedor findById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Usuario_reponedor usuario_reponedor = currentSession.get(Usuario_reponedor.class, id);

            return usuario_reponedor;
        }



        @Override
        public void save(Usuario_reponedor usuario_reponedor) {
            Session currentSession = entityManager.unwrap(Session.class);

            currentSession.saveOrUpdate(usuario_reponedor);

        }

        @Override
        public void deleteById(int id) {
            Session currentSession = entityManager.unwrap(Session.class);

            Query<Usuario_reponedor> theQuery = currentSession.createQuery("delete from Usuario Reponedor where id=:idUser");

            theQuery.setParameter("idUser", id);
            theQuery.executeUpdate();

        }
    }
}
