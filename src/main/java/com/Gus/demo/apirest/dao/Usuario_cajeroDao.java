package com.Gus.demo.apirest.dao;

import com.Gus.demo.apirest.entity.Usuario_cajero;
import java.util.List;

public interface Usuario_cajeroDao {

    public List<Usuario_cajero> findAll();
    public Usuario_cajero findById(int id);

    public void save(Usuario_cajero usuario_cajero);

    public void deleteById(int id);
}
