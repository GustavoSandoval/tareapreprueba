package com.Gus.demo.apirest.service;

import com.Gus.demo.apirest.entity.Merma;
import com.Gus.demo.apirest.entity.Usuario_cajero;

import java.util.List;

public interface Usuario_cajero_service {

    public List<Usuario_cajero> findAll();

    public Usuario_cajero findById(int id);

    public void save(Usuario_cajero usuario_cajero);

    public void deleteById(int id);
}
