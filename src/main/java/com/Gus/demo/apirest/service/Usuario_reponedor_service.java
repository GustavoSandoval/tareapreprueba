package com.Gus.demo.apirest.service;


import com.Gus.demo.apirest.entity.Usuario_reponedor;

import java.util.List;

public interface Usuario_reponedor_service {

    public List<Usuario_reponedor> findAll();

    public Usuario_reponedor findById(int id);

    public void save(Usuario_reponedor usuario_reponedor);

    public void deleteById(int id);
}
