package com.Gus.demo.apirest.service;

import com.Gus.demo.apirest.entity.Merma;
import java.util.List;

public interface MermaService {

    public List<Merma> findAll();

    public Merma findById(int id);

    public void save(Merma merma);

    public void deleteById(int id);

}
