package com.Gus.demo.apirest.service;


import com.Gus.demo.apirest.entity.Producto;

import java.util.List;

public interface ProductoService {

    public List<Producto> findAll();

    public Producto findById(int id);

    public void save(Producto producto);

    public void deleteById(int id);

}
